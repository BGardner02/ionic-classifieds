var img = angular.module('starter.controllers', ['ionic'])

.controller('AppCtrl', function($scope, $ionicModal, $timeout) {

  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //$scope.$on('$ionicView.enter', function(e) {
  //});

  // Form data for the login modal
  $scope.loginData = {};

  // Create the login modal that we will use later
  $ionicModal.fromTemplateUrl('templates/login.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.modal = modal;
  });

  // Triggered in the login modal to close it
  $scope.closeLogin = function() {
    $scope.modal.hide();
  };

  // Open the login modal
  $scope.login = function() {
    $scope.modal.show();
  };

  // Perform the login action when the user submits the login form
  $scope.doLogin = function() {
    console.log('Doing login', $scope.loginData);

    // Simulate a login delay. Remove this and replace with your login
    // code if using a login system
    $timeout(function() {
      $scope.closeLogin();
    }, 1000);
  };
})

.controller('PlaylistsCtrl', function($scope) {
  $scope.playlists = [
    { title: 'Appliances', id: 1 },
    { title: 'Auto Parts', id: 2 },
    { title: 'Books', id: 3 },
    { title: 'Cell Phones', id: 4 },
    { title: 'Computers', id: 5 },
    { title: 'Electronics', id: 6 },
    { title: 'Free', id: 8},
    { title: 'Furniture', id: 9},
    { title: 'Garage Sale', id: 10},
    { title: 'Household', id: 11},
    { title: 'Jewelry', id: 12},
    { title: 'Materials', id: 13},
    { title: 'Music Instruments', id: 14},
    { title: 'Video Gaming', id: 15}
  ];
})

.controller('PlaylistCtrl', function($scope, $stateParams) {
});

img.controller("ExampleController", function($scope) {
 
    $scope.images = [];
 
    $scope.loadImages = function() {
        for(var i = 0; i < 100; i++) {
            $scope.images.push({id: i, src: "/img/sale-1149344_640.jpg"});
        }
    }
 
});
